'use strict'

/*
 * Function to disable the gpu option initially.
 */
function disable_num_gpus_input(){
    let num_gpus_input = $('#num_gpus');
    num_gpus_input.attr('disabled', 'disabled');
    $('#num_gpus').val(0);
}



/*
 *Function to enable the gpu input.
 */
function enable_num_gpus_input(){
    let num_gpus_input = $('#num_gpus');
    num_gpus_input.removeAttr('disabled');
}


/*
 * Function to handle the change event in the custom_queue select dropdown
 */

function custom_queue_change_handler(selected_queue)
{
    let no_gpu_queue = ["normal"];
    let queue_name = selected_queue[0].value;
    if(no_gpu_queue.indexOf(queue_name)>-1){
    disable_num_gpus_input();
    $('#num_gpus').val(0);
}
else {
    enable_num_gpus_input();
}
};

/*
 * Invoke the functions when the page loads.
 */
$(document).ready(function(){

    $('select').find('option[value=interactive]').attr('selected','selected');

    //Initially set the num_gpus to 0 for default Interactive partition.
    $('#num_gpus').val(0);

    let queue = $('#batch_connect_session_context_custom_queue');

    //Handles the change event.
    queue.change(function(){
        custom_queue_change_handler(queue);
    })
});
